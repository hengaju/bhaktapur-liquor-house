class CreateSizes < ActiveRecord::Migration[6.0]
  def change
    create_table :sizes do |t|
      t.string :size
      t.integer :no_of_units

      t.timestamps
    end
  end
end
