class CreateCompanies < ActiveRecord::Migration[6.0]
  def change
    create_table :companies do |t|
      t.string :name
      t.date :estd
      t.text :description
      t.string :address

      t.timestamps
    end
  end
end
