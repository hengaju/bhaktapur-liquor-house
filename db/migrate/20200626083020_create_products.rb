class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :name
      t.integer :alchohol_percent
      t.integer :whole_sale_price
      t.integer :retail_price
      t.integer :mrp
      t.integer :dealer_price
      t.integer :distributer_price
      t.integer :total_quantity
      t.references :company, foreign_key: true
      t.references :size, foreign_key: true
      t.references :product_category, foreign_key: true
      t.timestamps
    end
  end
end
