class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.date :delivery_date
      t.integer :total_cost
      t.date :order_date
      t.string :remarks
      t.string :payment_method
      t.integer :invoice_no
      t.integer :status
      t.text :description
      t.integer :quantity
      t.integer :unit_price
      t.references :customer, foreign_key: true
      t.references :product, foreign_key: true
      t.timestamps
    end
  end
end
