class CreateCustomers < ActiveRecord::Migration[6.0]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :shop_name
      t.string :city
      t.string :street
      t.string :province
      t.integer :postal_code
      t.integer :primary_contact
      t.integer :secondary_contact
      t.integer :vat_no
      t.string :email

      t.timestamps
    end
  end
end
