# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_26_083147) do

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.date "estd"
    t.text "description"
    t.string "address"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "customer_products", force: :cascade do |t|
    t.integer "price"
    t.integer "customer_id"
    t.integer "product_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["customer_id"], name: "index_customer_products_on_customer_id"
    t.index ["product_id"], name: "index_customer_products_on_product_id"
  end

  create_table "customers", force: :cascade do |t|
    t.string "name"
    t.string "shop_name"
    t.string "city"
    t.string "street"
    t.string "province"
    t.integer "postal_code"
    t.integer "primary_contact"
    t.integer "secondary_contact"
    t.integer "vat_no"
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "orders", force: :cascade do |t|
    t.date "delivery_date"
    t.integer "total_cost"
    t.date "order_date"
    t.string "remarks"
    t.string "payment_method"
    t.integer "invoice_no"
    t.integer "status"
    t.text "description"
    t.integer "quantity"
    t.integer "unit_price"
    t.integer "customer_id"
    t.integer "product_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["customer_id"], name: "index_orders_on_customer_id"
    t.index ["product_id"], name: "index_orders_on_product_id"
  end

  create_table "product_categories", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.integer "alchohol_percent"
    t.integer "whole_sale_price"
    t.integer "retail_price"
    t.integer "mrp"
    t.integer "dealer_price"
    t.integer "distributer_price"
    t.integer "total_quantity"
    t.integer "company_id"
    t.integer "size_id"
    t.integer "product_category_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_products_on_company_id"
    t.index ["product_category_id"], name: "index_products_on_product_category_id"
    t.index ["size_id"], name: "index_products_on_size_id"
  end

  create_table "sizes", force: :cascade do |t|
    t.string "size"
    t.integer "no_of_units"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "customer_products", "customers"
  add_foreign_key "customer_products", "products"
  add_foreign_key "orders", "customers"
  add_foreign_key "orders", "products"
  add_foreign_key "products", "companies"
  add_foreign_key "products", "product_categories"
  add_foreign_key "products", "sizes"
end
