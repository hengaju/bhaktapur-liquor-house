Rails.application.routes.draw do
  resources :customer_products
  resources :products
  resources :product_categories
  resources :companies
  resources :sizes
  resources :orders
  resources :customers
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
