json.extract! product, :id, :name, :alchohol_percent, :whole_sale_price, :retail_price, :mrp, :dealer_price, :distributer_price, :total_quantity, :created_at, :updated_at
json.url product_url(product, format: :json)
