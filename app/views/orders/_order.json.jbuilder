json.extract! order, :id, :delivery_date, :total_cost, :order_date, :status, :remarks, :payment_method, :invoice_no, :status, :description, :quantity, :unit_price, :created_at, :updated_at
json.url order_url(order, format: :json)
