json.extract! customer, :id, :name, :shop_name, :city, :street, :province, :postal_code, :primary_contact, :secondary_contact, :vat_no, :email, :created_at, :updated_at
json.url customer_url(customer, format: :json)
