json.extract! size, :id, :size, :no_of_units, :created_at, :updated_at
json.url size_url(size, format: :json)
