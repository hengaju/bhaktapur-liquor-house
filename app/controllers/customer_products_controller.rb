class CustomerProductsController < ApplicationController
  before_action :set_customer_product, only: [:show, :edit, :update, :destroy]

  # GET /customer_products
  # GET /customer_products.json
  def index
    @customer_products = CustomerProduct.all
  end

  # GET /customer_products/1
  # GET /customer_products/1.json
  def show
  end

  # GET /customer_products/new
  def new
    @customer_product = CustomerProduct.new
  end

  # GET /customer_products/1/edit
  def edit
  end

  # POST /customer_products
  # POST /customer_products.json
  def create
    @customer_product = CustomerProduct.new(customer_product_params)

    respond_to do |format|
      if @customer_product.save
        format.html { redirect_to @customer_product, notice: 'Customer product was successfully created.' }
        format.json { render :show, status: :created, location: @customer_product }
      else
        format.html { render :new }
        format.json { render json: @customer_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customer_products/1
  # PATCH/PUT /customer_products/1.json
  def update
    respond_to do |format|
      if @customer_product.update(customer_product_params)
        format.html { redirect_to @customer_product, notice: 'Customer product was successfully updated.' }
        format.json { render :show, status: :ok, location: @customer_product }
      else
        format.html { render :edit }
        format.json { render json: @customer_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customer_products/1
  # DELETE /customer_products/1.json
  def destroy
    @customer_product.destroy
    respond_to do |format|
      format.html { redirect_to customer_products_url, notice: 'Customer product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer_product
      @customer_product = CustomerProduct.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def customer_product_params
      params.require(:customer_product).permit(:price)
    end
end
