class Product < ApplicationRecord
		belongs_to :product_category
	belongs_to :company
	belongs_to :size
	has_many :orders
	has_many :customer_products
end
