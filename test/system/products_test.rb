require "application_system_test_case"

class ProductsTest < ApplicationSystemTestCase
  setup do
    @product = products(:one)
  end

  test "visiting the index" do
    visit products_url
    assert_selector "h1", text: "Products"
  end

  test "creating a Product" do
    visit products_url
    click_on "New Product"

    fill_in "Alchohol percent", with: @product.alchohol_percent
    fill_in "Dealer price", with: @product.dealer_price
    fill_in "Distributer price", with: @product.distributer_price
    fill_in "Mrp", with: @product.mrp
    fill_in "Name", with: @product.name
    fill_in "Retail price", with: @product.retail_price
    fill_in "Total quantity", with: @product.total_quantity
    fill_in "Whole sale price", with: @product.whole_sale_price
    click_on "Create Product"

    assert_text "Product was successfully created"
    click_on "Back"
  end

  test "updating a Product" do
    visit products_url
    click_on "Edit", match: :first

    fill_in "Alchohol percent", with: @product.alchohol_percent
    fill_in "Dealer price", with: @product.dealer_price
    fill_in "Distributer price", with: @product.distributer_price
    fill_in "Mrp", with: @product.mrp
    fill_in "Name", with: @product.name
    fill_in "Retail price", with: @product.retail_price
    fill_in "Total quantity", with: @product.total_quantity
    fill_in "Whole sale price", with: @product.whole_sale_price
    click_on "Update Product"

    assert_text "Product was successfully updated"
    click_on "Back"
  end

  test "destroying a Product" do
    visit products_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Product was successfully destroyed"
  end
end
